This folder contains various utility and software setup scripts.   
To run these scripts add the scripts folder to PATH environmental variable.

Description and usage of scripts in the folders are given below.

# utilities
**svgExport**   
Perl script for batch exporting of svg files to pdf, png or eps.   
For usage information type following command in a terminal
```
svgExport -h
```

**syncFrmGit**   
Perl script for pulling multiple git projects from remotes specified in a file.   
For usage information type following command in a terminal
```
syncFrmGit -h
```

**wiki2beamer**   
Python script for creating latex beamer source files from a wiki-like text file.   
For more information check http://wiki2beamer.sourceforge.net/

**wiki2pdf**   
Wrapper bash script around wiki2beamer,
for directly generating pdf files from a wiki-like text file.   
*Usage:*
```
wiki2pdf $filename
```


# todo
Add installation and setup scripts for:
    ngsim-gschem toolflow

# License
wiki2beamer is made available under GPLv2 license.   
All other files are made available under MIT License, which can be read from the LICENSE file in this folder.
